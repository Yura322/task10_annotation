package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Student {

    private static Logger log = LogManager.getLogger(Student.class);

    @InfoStudent(firstName = "Dmytro")
    private String firstName;
    @InfoStudent(lastName = "Kaminsckyi")
    private String lastName;
    @InfoStudent(age = 21)
    private int age;
    private int stipendPerDay = 50;

    private int countStipendPerMonth() {
        int day = 30;
        return stipendPerDay * day;
    }

    private double countStipendPerMonthDouble(int stipendPerDay) {
        int day = 30;
        return (double) stipendPerDay * day;
    }

    private void showStipendPerMonth() {
        int day = 30;
        log.info("Show : " + stipendPerDay * day);
    }

    private void showWords(String... words) {
        for (String s : words) {
            log.info(s);
        }
    }

    private void countNumbers(String s, int... numbers) {
        int count = 0;
        for (Integer i : numbers) {
            log.info(s + " : " + i);
            count += i;
        }
        log.info("Result : " + count);
    }
}
